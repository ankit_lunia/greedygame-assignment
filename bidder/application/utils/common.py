from flask import Flask, current_app, request
import os
from werkzeug.exceptions import Unauthorized, BadRequest

def create_app(__name__):
    app = Flask(__name__)

    if os.environ.get("ENVIRONMENT", "dev").lower() == "production":
        app.config.from_object('application.config.prod.ProductionConfig')
    else:
        app.config.from_object('application.config.dev.DevelopmentConfig')

    return app

def check_headers():
    print(request.headers)
    api_key = request.headers.get('Api-Key')
    print(api_key)
    try:
        if not api_key:
            raise BadRequest('No api key supplied')

        if api_key != current_app.config.get('API_KEY'):
            raise Unauthorized('Invalid authentication!')
    except Exception:
        print('Error in auth headers!')
        raise