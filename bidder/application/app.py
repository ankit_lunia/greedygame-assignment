import os
import sys
sys.path.append(os.getcwd())

from flask import Flask
from application.utils.common import create_app, check_headers
from application.blueprints.bidder.views import blueprint as bidder_blueprint

app = create_app(__name__)
app.app_context().push()

app.register_blueprint(bidder_blueprint, url_prefix='/')

app.before_request(check_headers)


if __name__ == '__main__':
    app.run()