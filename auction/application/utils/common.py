from flask import Flask
import os

def create_app(__name__):
    app = Flask(__name__)

    if os.environ.get("ENVIRONMENT", "dev").lower() == "production":
        app.config.from_object('application.config.prod.ProductionConfig')
    else:
        app.config.from_object('application.config.dev.DevelopmentConfig')

    return app