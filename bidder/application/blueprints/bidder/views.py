from flask import jsonify, Blueprint, request, render_template, current_app
import uuid
import random
import time

blueprint = Blueprint('bidder_api', __name__)

@blueprint.route('/ad-slot/<string:adPlacementId>', methods=['GET'])
def get_ad_biddings(adPlacementId):
    status_code = 200
    
    bid_price = random.randint(0,20)
    ad_id = uuid.uuid4()

    if bid_price in [0,1,2,3]:
        status_code = 204
    return jsonify({'AdId': ad_id, 'bidprice': bid_price}), status_code
