from flask import jsonify, Blueprint, request, render_template, current_app
import uuid
import random
from application.client.bidding import Bidding

blueprint = Blueprint('auction_api', __name__)


@blueprint.route('/ad-slot/<string:adPlacementId>', methods=['GET'])
def get_auction_details(adPlacementId):
    status_code = 200
    biddings = []
    
    res = [Bidding().make_request('/ad-slot/{}'.format(adPlacementId)) for i in range(0, current_app.config.get('BIDDIG_INSTANCES'))]

    for r in res:
        try:
            if r.result().status_code == 200:
                biddings.append(r.result().json())
            else:
                raise(r.result().status_code)
        except Exception as e:
            print(r.exception)
    
    biddings = sorted(biddings, key = lambda k:k['bidprice'], reverse=True)[0:1]

    if not biddings:
        status_code = 204

    return jsonify({'res':biddings}), status_code
