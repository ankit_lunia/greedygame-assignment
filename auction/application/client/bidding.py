from requests_futures.sessions import FuturesSession
from flask import current_app

class Bidding():
    def __init__(self):
        self.session = FuturesSession()
        self.url = current_app.config.get('BIDDING_URL')
    
    def make_request(self, url, params={'key1':'val1'}, json={}, request_type='get'):
        api_url = self.url + url
        headers = {'Content-Type': 'application/json', 'Api-Key': current_app.config.get('API_KEY')}
        
        response = self.session.get(api_url, params=params, json=json, timeout=0.2, headers=headers)

        return response